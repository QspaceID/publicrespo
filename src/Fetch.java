import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Fetch {
	public static void main(String args[]) {
		try {
			Class.forName("org.postgresql.Driver");
			Connection conn = DriverManager.getConnection("jdbc:postgresql://192.168.1.155:5442/postgres","postgres","admin");
			PreparedStatement ps = conn.prepareStatement("select * from testcase order by tcid desc limit to 1");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				System.out.println(rs.getInt(1) + " " +rs.getString(2) + " " +rs.getString(3));
			}
		}catch(Exception ex) {
			System.out.println(ex);
		}
	}
}
