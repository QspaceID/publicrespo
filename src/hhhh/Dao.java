package hhhh;

import org.bson.BSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;

public class Dao {
	static String _VERSION = "_version";
	public static void insert(DBCollection collection,DBObject object){
		if(object.containsField(_VERSION)) 
			throw new IllegalArgumentException();
		object.put(_VERSION, 1);
		collection.insert(object,WriteConcern.SAFE);
	}
	
	public static void update(DBCollection collection,DBObject object) throws Exception {
			if(!object.containsField(_VERSION)) throw new IllegalArgumentException();
			int baseVersion = (int) object.get(_VERSION);{
				DBObject base = collection.findOne(new BasicDBObject("_id",getId(object)));
				if (base==null) {
					throw new IllegalArgumentException();
				}
				Object bV = base.get(_VERSION);
				if(bV instanceof Integer) {
					if(baseVersion != (Integer)bV){
						throw new Exception();
					}
				} else {
					throw new Exception();
				}
				DBCollection shadow = getShadowCollection(collection);
				base.put("_id", new BasicDBObject("_id", getId(base)).append(
						_VERSION, baseVersion));
				WriteResult r = shadow.insert(base, WriteConcern.SAFE);
				//r.getLastError().throwOnError();
				}
			try {
				object.put(_VERSION, baseVersion + 1);
				DBObject found = collection.findAndModify(new BasicDBObject("_id",
						getId(object)).append(_VERSION, baseVersion), object);

				if (found == null) {
					// document has changed in the mean-time. get the latest version
					// again
					DBObject base = collection.findOne(new BasicDBObject("_id",
							getId(object)));
					if (base == null) {
						throw new IllegalArgumentException(
								"document to update not found in collection");
					}
					throw new Exception();
				}

			} catch (RuntimeException e) {
				object.put(_VERSION, baseVersion);
				throw e;
			}
		}
	  static Object getId(BSONObject object) {
		  
		return object.get("_id");
	}
	  
	  	static Integer getVersion(BSONObject o) {
			return (Integer) o.get(_VERSION);
		}

	
	  static DBCollection getShadowCollection(DBCollection collection) {
		
		return collection.getCollection("vermongo");
	}
	
	static DBObject getOldVersion(DBCollection c, Object id, int versionNumber) {

		BasicDBObject query = new BasicDBObject("_id", new BasicDBObject("_id",
				id).append("_version", versionNumber));

		DBObject result = getShadowCollection(c).findOne(query);
		if (result == null)
			return null;
		result.put("_id", ((BasicDBObject) getId(result)).get("_id"));
		return result;
	}
}
