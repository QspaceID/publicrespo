

import hhhh.Dao;
import hhhh.Database;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String name=request.getParameter("name");
			Dao dao = new Dao();
			MongoClient mongo = new MongoClient("localhost",27017);
			@SuppressWarnings("deprecation")
			DB db = mongo.getDB("test");
			DBCollection collection = db.getCollection("new");
			DBObject object = new BasicDBObject().append("name", name);
			dao.insert(collection, object);
	}

}
