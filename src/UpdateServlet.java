

import hhhh.Dao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doPut(request,response);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		   String name= req.getParameter("name");
		   MongoClient mongo = new MongoClient("localhost",27017);
		   	@SuppressWarnings("deprecation")
			DB db = mongo.getDB("test");
		   	DBCollection coll = db.getCollection("new");
		   	BasicDBObject object = new BasicDBObject("name",new BasicDBObject("$exists",true)).append("name", name);
			DBCursor cursor = coll.find(object);
			while(cursor.hasNext()) {
				System.out.println(cursor.next());
			}
			try {
				Dao.update(coll, object);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

}
